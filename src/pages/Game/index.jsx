import React, {
  Fragment,
  useCallback,
  useEffect,
  useState,
  useMemo,
} from "react";
import "./index.css";
import Controls from "../../components/Control";
import Main from "../../components/Main";
import axios from "axios";
import { useDispatch } from "react-redux";

const Game = () => {
  const [count, setCount] = useState(0);
  const [count2, setCount2] = useState(0);

  // const total = useMemo(() => {
  //   return 666 * 6666 * 6666 + 20;
  // }, [total]);

  const dispatch = useDispatch();

  const fetchNewDeck = useCallback(() => {
    axios({
      method: "GET",
      url: "https://deckofcardsapi.com/api/deck/new/",
    })
      .then((res) => {
        dispatch({ type: "SET_DECK_CARD", payload: res.data });
        localStorage.setItem("deck_id", res.data.deck_id);
      })
      .catch((err) => {
        console.log(err);
      });
  }, [dispatch]);

  const shuffleDeck = useCallback(
    (id) => {
      axios({
        method: "GET",
        url: `https://deckofcardsapi.com/api/deck/${id}/shuffle/`,
      })
        .then((res) => {
          dispatch({ type: "SET_DECK_CARD", payload: res.data });
        })
        .catch((err) => {
          console.log(err);
        });
    },
    [dispatch]
  );
  //Didmount, didupdate, willunmount

  //didmount: mảng dependencies rỗng
  useEffect(() => {
   // console.log("useEffect Didmount");

    //call api
    const deckId = localStorage.getItem("deck_id");
    if (deckId) shuffleDeck(deckId);
    else fetchNewDeck();
  }, []);

  //did update: chỉ chạy lại khi count2 thay đổi
  useEffect(() => {
  //  console.log(count2);
  }, [count2]);

  return (
    <Fragment>
      <button onClick={() => setCount(count + 1)}>Set Count</button>
      <button onClick={() => setCount2(count2 + 1)}>Set Count 2</button>
      <Controls />
      <Main />
    </Fragment>
  );
};

export default Game;
