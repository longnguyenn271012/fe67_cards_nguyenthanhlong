const initialState = {
  isRevealed: false,
  isDrawed: false,
  countGame: 0,
};

const reducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case "SET_REVEAL":
      state.isRevealed = payload;
      return { ...state };
    case "SET_DRAW":
      state.isDrawed = payload;
      return { ...state };
    case "SET_COUNT_GAME":
      let counter = state.countGame;
      counter = +counter + payload;
      state.countGame = counter;
      return { ...state };
    default:
      return state;
  }
};

export default reducer;
