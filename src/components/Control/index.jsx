import React, { memo, useCallback, useEffect } from "react";
import { useSelector } from "react-redux";
import axios from "axios";
import { useDispatch } from "react-redux";

const Control = () => {
  // useEffect(() => {
  //   console.log("Control Render!!!");
  // });

  const players = useSelector((state) => {
    return state.player.playerList;
  });

  const deckCard = useSelector((state) => {
    return state.card.deckCard;
  });

  const isRevealed = useSelector((state) => {
    return state.status.isRevealed;
  });

  const isDrawed = useSelector((state) => {
    return state.status.isDrawed;
  });

  const countGame = useSelector((state) => {
    return state.status.countGame;
  });

  const dispatch = useDispatch();

  const handleShuffleCards = useCallback(async () => {
    const playerList = [...players];
    for (let p of playerList) {
      p.cards.splice(0, p.cards.length);
    }
    dispatch({ type: "SET_PLAYERS", payload: playerList });
    dispatch({ type: "SET_REVEAL", payload: false });
    dispatch({ type: "SET_DRAW", payload: false });

    // Neu du 5 game roi thi reset countGame
    // reset totalPoint cua tung nguoi
    if (countGame === 5) {
      dispatch({ type: "SET_COUNT_GAME", payload: -5 });
      for (let p of playerList) {
        p.totalPoint = 25000;
      }
      dispatch({ type: "SET_PLAYERS", payload: playerList });
    }

    try {
      const res = await axios({
        method: "GET",
        url: `https://deckofcardsapi.com/api/deck/${deckCard.deck_id}/shuffle/`,
      });

      dispatch({ type: "SET_DECK_CARD", payload: res.data });
    } catch (err) {
      console.error(err);
    }
  }, [dispatch, players, deckCard]);

  const handleFetchCards = useCallback(async () => {
    if (!isDrawed) {
      try {
        const res = await axios({
          method: "GET",
          url: `https://deckofcardsapi.com/api/deck/${deckCard.deck_id}/draw/?count=12`,
        });
        //res.data.cards là mảng đag có 12 lá, giờ cần chia đều cho 4 người

        //chia 12 lá bài cho 4 players
        const playerList = [...players];
        for (let i in res.data.cards) {
          const playerIndex = +i % playerList.length;
          playerList[playerIndex].cards.push(res.data.cards[i]);
          // i = 0 => playerIndex = 0
          // i = 1 => playerIndex = 1
          // i = 2 => playerIndex = 2
          // i = 3 => playerIndex = 3
          // i = 4 => playerIndex = 0
        }

        //dispatch nguyên playerList mới lên reducer
        dispatch({ type: "SET_DRAW", payload: true });
        dispatch({ type: "SET_PLAYERS", payload: playerList });
      } catch (err) {
        console.error(err);
      }
    }
  }, [dispatch, players, deckCard]);

  const handleRevealCards = useCallback(async () => {
    if (!isRevealed) {
      dispatch({ type: "SET_COUNT_GAME", payload: 1 });
      checkResult();
    }

    dispatch({ type: "SET_REVEAL", payload: true });
    
    if (countGame === 4) {
      checkTotalResult();
    }
  }, [dispatch, countGame, players]);

  const checkResult = useCallback(() => {
    const playerList = [...players];

    const winners = [];

    // check special case
    for (let i of playerList) {
      if (checkSpecialCase(i.cards)) winners.push(i);
    }

    // Nếu ko có special case  cộng điểm từng player rồi tìm max
    if (winners.length === 0) {
      // Mảng lưu điểm từng người
      let result = [];
      let max = 0;
      for (let p of playerList) {
        let sum = 0;
        for (let c of p.cards) {
          sum += convertCardValue(c.value);
        }
        sum = sum % 10;
        result.push({ player: p, sum: sum });
        // Tìm max
        if (sum >= max) max = sum;
      }

      // Tìm người thắng
      for (let r of result) {
        if (r.sum === max) winners.push(r.player);
      }
    }

    // update điểm cho người thắng
    // Trừ trước mỗi người 5k
    // Sau đó cộng 2000 tổng chia cho số người win
    for (let p of playerList) {
      p.totalPoint -= 5000;
      for (let w of winners) {
        if (p.username === w.username) {
          p.totalPoint += 20000 / winners.length;
          break;
        }
      }
    }

    // dispatch điểm lên store cập nhật playerList
    dispatch({ type: "SET_PLAYERS", payload: playerList });

    // Kiem tra du 5 van chua.
    // du roi thi tim diem cao nhat
    // xem ai cao diem nhat thi alert
    // sau do set lai countGame = 0
  }, [dispatch, countGame, players]);

  const checkTotalResult = useCallback(() => {
    let totalWinners = [];
    let maxTotalPoint = 0;
    for (let p of players) {
      if (p.totalPoint >= maxTotalPoint) {
        maxTotalPoint = p.totalPoint;
      }
    }

    for (let p of players) {
      if (p.totalPoint === maxTotalPoint) {
        totalWinners.push(p);
      }
    }

    for (let w of totalWinners) {
      alert(w.username);
    }
    console.log(totalWinners);
    // alert("Winner(s): " + totalWinners.join(totalWinners.username + ", "));
  });

  // Nhận vào mảng cards của players, check bằng value
  // nếu cả 3 lá là K Q J thì là trường hợp đặc biệt
  const checkSpecialCase = (cards) => {
    //KING JACK QUEEN
    let count = 0;
    for (let i of cards) {
      if (i.value === "KING" || i.value === "QUEEN" || i.value === "JACK")
        count++;
    }

    if (count === 3) return true;
    else return false;
  };

  // Nhận vào value của card và return về number
  // number => number
  // K J Q => 10
  // ACE => 1
  const convertCardValue = (val) => {
    if (val === "KING" || val === "QUEEN" || val === "JACK") val = 10;
    else if (val === "ACE") val = 1;
    else val = +val;

    return val;
  };

  return (
    <div className="d-flex  justify-content-end container">
      <div className="border d-flex justify-content-center align-items-center px-2">
        <button className="btn btn-success mr-2" onClick={handleShuffleCards}>
          Shuffle
        </button>
        <button className="btn btn-info mr-2" onClick={handleFetchCards}>
          Draw
        </button>
        <button className="btn btn-primary mr-2" onClick={handleRevealCards}>
          Reveal
        </button>
      </div>
      <div className="d-flex">
        {players.map((player) => {
          return (
            <div key={player.username} className="border px-3 text-center">
              <p>{player.username}</p>
              <p> {player.totalPoint.toFixed(2)}</p>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default memo(Control);
